package spring.jparepositories.demoSpringJpaRepositories.bootstrap;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import spring.jparepositories.demoSpringJpaRepositories.model.Author;
import spring.jparepositories.demoSpringJpaRepositories.model.Book;
import spring.jparepositories.demoSpringJpaRepositories.repositories.AuthorRepository;
import spring.jparepositories.demoSpringJpaRepositories.repositories.BookRepository;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent >
{

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)
    {
      initData();
    }

    private void initData()
    {
        Author eric=new Author("Eric","Evans");
        Book ddd =new Book("Domain Driven Design","1234","Harper Collins");
        eric.getBooks().add(ddd);
        ddd.getAuthors().add(eric);

        authorRepository.save(eric);
        bookRepository.save(ddd);


        Author rod=new Author("Rod","Jhonson");
        Book boEjb =new Book("J2ee Developement without EJB","220010","Workx");
        rod.getBooks().add(boEjb);
        boEjb .getAuthors().add(rod);
        authorRepository.save(rod);
        bookRepository.save(boEjb);




    }
}


