package spring.jparepositories.demoSpringJpaRepositories.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import spring.jparepositories.demoSpringJpaRepositories.model.Author;
import spring.jparepositories.demoSpringJpaRepositories.repositories.AuthorRepository;

@Controller
public class AuthorController
{
private AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @RequestMapping(value ="/authors",method=RequestMethod.GET)

    public String getAuthor(Model model)
    {
      model.addAttribute("authors1",authorRepository.findAll());
      return "authors1";
    }



}
