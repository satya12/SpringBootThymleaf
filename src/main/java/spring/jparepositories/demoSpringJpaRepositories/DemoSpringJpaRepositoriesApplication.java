package spring.jparepositories.demoSpringJpaRepositories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringJpaRepositoriesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringJpaRepositoriesApplication.class, args);
	}
}
