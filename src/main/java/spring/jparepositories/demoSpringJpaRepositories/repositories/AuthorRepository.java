package spring.jparepositories.demoSpringJpaRepositories.repositories;

import org.springframework.data.repository.CrudRepository;
import spring.jparepositories.demoSpringJpaRepositories.model.Author;

public interface AuthorRepository extends CrudRepository<Author,Long>
{

}
