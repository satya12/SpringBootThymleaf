package spring.jparepositories.demoSpringJpaRepositories.repositories;

import org.springframework.data.repository.CrudRepository;
import spring.jparepositories.demoSpringJpaRepositories.model.Book;

public interface BookRepository extends CrudRepository<Book,Long> {
}
